# Projet pluri-disciplinaire - master I Miage

Création d'une application permettant d'analyser et de suivre la e-réputation de produits en ligne à partir de plusieurs sources (Tweeter, Amazon, YouTube).
Architecture du projet : client / serveur

# SERVEUR

Installer venv
NB: Les commandes d'installation de l'environnement virtuel pour python, varient selon le système d'exploitation. ici on travaille avec Linux.
sudo apt-get install python3-venv

Créer et activer un environnement virtuel
python3 -m venv my_venv
source my_venv/bin/activate

Installer les modules requis pour le serveur
après l'activation de l'environnement virtuel, se déplacer dans le dossier server:
cd server
Installer les modules requis :
pip3 install -r requirements.txt
Cette étape peut prendre du temps : entre 5 à 10 minutes selon les machines et la connexion internet

Si l'installation de Tensor Flow échoue, il faut le mettre à niveau:

python -m pip install --upgrade pip
pip install --upgrade tensorflow==2.0

Importer les clés pour les API
Décompresser l'archive api_keys.zip fourni en annexe, dans le repertoire /server
Attention: Après la décompression, un repertoire nommé api_keys et contenant les fichiers d'identification, doit apparaitre dans le répértoire /server.

Lancer le serveur
Toujours situé dans le dossier server et l'environnement virtuel activé, lancer le serveur :
python3 manage.py runserver

Vérifier le bon fonctionnement du serveur
Ouvrir un navigateur et y inscrire dans la barre de recherche :
localhost:8000/admin/
Vous tomberez sur une page de connexion. Les identifiants vous sont communiqués en annexe.

# CLIENT

- Ouvrir l'application RStudio et sélectionner File > Open Project
- Sélectionner le fichier **client** de notre projet et ouvrir *ppd.Rproj*

###### Installer les paquets

Copier coller le code ci-dessous dans la console de RStudio :

`install.packages(c("shinydashboard", "tm", "wordcloud", "RColorBrewer", "NLP", "RJSONIO", "dplyr", "shinyjs", "httr", "shinyWidgets", "reshape2", "ggplot2", "shinycssloaders"))`

*Cette étape peut prendre quelques temps en fonction de la machine et de la connexion internet.* 

###### Lancer l'application

Il faut s'assurer que tous les paquets ont bien été installés. Ensuite, cliquer sur Run App dans l'interface de RStudio.






