import tweepy
import json
from e_rep.models import Twitter

class Twitter_API:

    def __init__(self,predictor,credentials_file):
        self.predictor = predictor
        with open(credentials_file, "r") as file:
            cred = json.load(file)
            auth = tweepy.OAuthHandler(cred["CONSUMER_KEY"],cred["CONSUMER_SECRET"])
            auth.set_access_token(cred["ACCESS_TOKEN"],cred["ACCESS_SECRET"]) 
            self.api = tweepy.API(auth)
    
    def get_data(self,product, max_twitts=10, langue="en"):
        tweet_object = [status for status in tweepy.Cursor(self.api.search, q=product.name, lang = langue).items(max_twitts)]
        data =[]

        for tweet in tweet_object:
            data.append(
                Twitter(
                    product=product,
                    text=tweet.text,
                    sentiment=self.predictor.predict([tweet.text])[0],
                    favorite_count= tweet.favorite_count,
                    retweet_count= tweet.retweet_count,
                    date=tweet.created_at.date()
                )
            )

        Twitter.objects.bulk_create(data)
