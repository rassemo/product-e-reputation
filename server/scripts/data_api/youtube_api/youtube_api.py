from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
import os
import pickle
from langdetect import detect
import datefinder
from langdetect.lang_detect_exception import LangDetectException
from e_rep.models import Youtube

class Youtube_API:

    def __init__(self,predictor,secret_file):
        self.predictor = predictor
        with open(secret_file, 'r') as f:
            secret_key = f.read()
            self.service = build("youtube","v3",developerKey=secret_key)
                
       # search for videos by keyword
    def find_videos_by_keyword(self,**kwargs):
        videos = []
        results = self.service.search().list(**kwargs).execute()

        i = 0
        max_pages = 2
        while results and i < max_pages:
            videos.extend(results['items'])
    
            # Check if another page exists
            if 'nextPageToken' in results:
                kwargs['pageToken'] = results['nextPageToken']
                results = self.service.search().list(**kwargs).execute()
                i += 1
            else:
                break
    
        return videos

    # fetch comments of a given video ID
    def get_video_comments(self,**kwargs):
        comments = []
        try:
            results = self.service.commentThreads().list(**kwargs).execute()
            while results:
                for item in results['items']:
                    """
                        likeCount = item['snippet']['topLevelComment']['snippet']['likeCount']
                        totalReplyCount = item['snippet']['totalReplyCount']
                        publishedAt = item['snippet']['topLevelComment']['snippet']['publishedAt']
                    """
                    comment = item['snippet']['topLevelComment']['snippet']['textDisplay']
                    # keep only english language
                    if(detect(comment)=='en'):
                        comments.append(comment)
        
                if 'nextPageToken' in results:
                    kwargs['pageToken'] = results['nextPageToken']
                    results = self.service.commentThreads().list(**kwargs).execute()
                else:
                    break

        except (HttpError,LangDetectException) as e:
            # bad requests: comments disabled, language issues ...
            pass
                
        return comments

    # get statistics by video ID
    def get_video_stats(self,video_id):
        res = self.service.videos().list(part='statistics', id=video_id).execute()
        stats = {'likeCount':0,'viewCount':0}
        try:
            stats['likeCount'] = int( res['items'][0]['statistics']['likeCount'] )
            stats['viewCount'] = int( res['items'][0]['statistics']['viewCount'] )
        except KeyError:
            # one or more stats are not available
            pass

        return stats

    # search data for the given keyword
    def get_data(self,product):
        
        videos = self.find_videos_by_keyword(
            q=product.name,
            part='id,snippet',
            eventType='completed',
            type='video', 
            relevanceLanguage="en",
            maxResults=2
        )
        
        for item in videos:
            results = []

            video_id = item['id']['videoId']
            date = list(datefinder.find_dates(item['snippet']['publishedAt']))[0] 
            stats = self.get_video_stats(video_id)

            comments = self.get_video_comments(part='snippet', videoId=video_id, textFormat='plainText')
            for comment in comments:

                results.append(
                    Youtube(
                        product = product,
                        video_id = video_id,
                        text = comment,
                        sentiment = self.predictor.predict([comment])[0],
                        like_count = stats['likeCount'],
                        view_count = stats['viewCount'],
                        date = date
                    )
                )                 
    
            Youtube.objects.bulk_create(results)
