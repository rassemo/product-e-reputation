from selectorlib import Extractor
import requests
import json
import os
import datefinder
from e_rep.models import Amazon, Scraping_Url

class Amazon_Scraper:

	def __init__(self,predictor,selectors_file):

		self.base_url = "https://www.amazon.com"
		self.extractor = Extractor.from_yaml_file(selectors_file)
		self.predictor = predictor

	def scrape(self,url):

		# HTTP request headers
		headers = {
			'authority': 'www.amazon.com',
			'pragma': 'no-cache',
			'cache-control': 'no-cache',
			'dnt': '1',
			'upgrade-insecure-requests': '1',
			'user-agent': 'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
			'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
			'sec-fetch-site': 'none',
			'sec-fetch-mode': 'navigate',
			'sec-fetch-dest': 'document',
			'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
		}

		# Download the page using requests
		res = requests.get(url, headers=headers)

		# check response status
		if res.status_code != 200:
			print("HTTP Error : status code %d - %s"%(res.status_code,url))
			return {}

		# Pass the HTML of the page and extract the data
		return self.extractor.extract(res.text)

	def get_data(self,product):
		
		for item in Scraping_Url.objects.filter(site='amazon',product_name=product.name).values('url'):
			results = []
			url = item['url']
			while(True):

				data = self.scrape(url)

				if data.get('reviews',False):

					for review in data['reviews']:
						review_text = review['title'] +'. '+review['content']
						sentiment = self.predictor.predict([review_text])[0]
						date = list(datefinder.find_dates(review['date']))[0]
						results.append(
							Amazon(
								product = product,
								text = review_text,
								sentiment = sentiment,
								rating = int(review['rating'][0]),
								variant = review['variant'],
								date = date
							)
						)

				if(data['next_page']!=None):
					url = self.base_url+data['next_page']
				else:
					break

			Amazon.objects.bulk_create(results)
