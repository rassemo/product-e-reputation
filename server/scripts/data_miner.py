from e_rep.models import Features, GoodText
from .db_utils import SQLite_DB
from .word_freq import Word_Freq
from datetime import date
import json

db = SQLite_DB()
wf = Word_Freq()

def save_features(product):
	
	f = Features(product=product)

	rows = db.exec_sql('q1',product.id,1)
	# positive tweets count
	f.twitter_text_count_pos = rows[0]['twitter_text_count']
	# sum fo likes, positive tweets
	f.twitter_like_sum_pos= rows[0]['twitter_like_sum']
	# sum fo retweets, positive tweets
	f.twitter_retweet_sum_pos= rows[0]['twitter_retweet_sum']

	rows = db.exec_sql('q1',product.id,-1)
	# neagtive tweets count
	f.twitter_text_count_neg= rows[0]['twitter_text_count']
	# sum fo likes, nagative tweets
	f.twitter_like_sum_neg= rows[0]['twitter_like_sum']
	# sum fo retweets, negative tweets
	f.twitter_retweet_sum_neg= rows[0]['twitter_retweet_sum']

	rows = db.exec_sql('q2',product.id,1)
	# youtube positive comments count
	f.youtube_text_count_pos= rows[0]['youtube_text_count']

	rows = db.exec_sql('q2',product.id,-1)
	# youtube negative comments count
	f.youtube_text_count_neg= rows[0]['youtube_text_count']

	rows = db.exec_sql('q3',product.id)
	# youtube: sum of likes & views of videos
	f.youtube_like_count= rows[0]['youtube_like_count']
	f.youtube_view_count= rows[0]['youtube_view_count']

	rows = db.exec_sql('q4',product.id,1)
	# amazon positive review count
	f.amazon_text_count_pos= rows[0]['amazon_text_count']
	# rating average (out of 5) for positive reviews
	f.amazon_rating_avg_pos= rows[0]['amazon_rating_avg']

	rows = db.exec_sql('q4',product.id,-1)
	# amazon negative review count
	f.amazon_text_count_neg= rows[0]['amazon_text_count']
	# rating average (out of 5) for negative reviews
	f.amazon_rating_avg_neg= rows[0]['amazon_rating_avg']

	# date create
	f.date = date.today()

	f.save()

def save_text(product):

	t = GoodText(product=product)

	rows = db.exec_sql('q5',product.id,1)
	# most positive tweet (max likes)
	t.twitter_most_pos_text= rows[0]['twitter_popular_text']
	# like count of most positive tweet
	t.twitter_most_pos_text_likes= rows[0]['max_likes']

	rows = db.exec_sql('q5',product.id,-1)
	# most negative tweet (max likes)
	t.twitter_most_neg_text= rows[0]['twitter_popular_text']
	# like count of most negative tweet
	t.twitter_most_neg_text_likes= rows[0]['max_likes']

	rows = db.exec_sql('q6',product.id,1)
	# most positive review (max likes)
	t.amazon_most_pos_text= rows[0]['amazon_popular_text']
	# like count of most positive review
	t.amazon_most_pos_text_rating= rows[0]['max_rating']

	rows = db.exec_sql('q6',product.id,-1)
	# most negative review (max likes)
	t.amazon_most_neg_text= rows[0]['amazon_popular_text']
	# like count of most negative review
	t.amazon_most_neg_text_rating = rows[0]['max_rating']
	
	# positive word cloud (all data sources)
	p = product.id
	s = 1
	rows = db.exec_sql('q7',p,s,p,s,p,s)
	w_cloud = wf.get_freq(rows)
	t.pos_word_cloud = json.dumps(w_cloud)

	# negative word cloud (all data sources)
	s = -1
	rows = db.exec_sql('q7',p,s,p,s,p,s)
	w_cloud = wf.get_freq(rows)
	t.neg_word_cloud = json.dumps(w_cloud)

	t.save()

