from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
import pickle
#import ktrain

class Simple_Pred:

	def __init__(self,model_file_h5, tokenizer_file_pk):

		self.model = load_model(model_file_h5)
		with open(tokenizer_file_pk,'rb') as handle:
			self.tokenizer = pickle.load(handle)

	def predict(self,text_list):

		z = pad_sequences(
			self.tokenizer.texts_to_sequences(text_list),
			maxlen=self.model.get_layer(index=0).get_config()['input_length']
		)

		preds = map(
			lambda p: 1 if p[1] >= 0.5 else -1,
			self.model.predict(z)
		)

		return list(preds)
	
class BERT_Pred:

	def __init__(self,model_directory):
		self.model = ktrain.load_predictor(model_directory)

	def predict(self,text_list):
		preds = map(
			lambda p: 1 if p == 'pos' else -1,
			self.model.predict(text_list)
		)

		return list(preds)

"""
	ktrain => tensorflow 2.0
	python -m pip install --upgrade pip
	pip install --upgrade tensorflow==2.0
"""
