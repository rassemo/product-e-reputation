from django.contrib import admin

# Register your models here.
from .models import Product, Twitter, Youtube, Amazon, Scraping_Url, Features, GoodText 
from scripts import Data_Loader
import threading

data_loader = Data_Loader()

class ProductAdmin(admin.ModelAdmin):

	list_display = ("name",)
	actions = ("update_data",)

	def update_data(modeladmin,request,queryset):
		thread = threading.Thread(target = data_loader.process_many, args = (queryset,))
		thread.start()

class Scraping_UrlAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Scraping_Url._meta.fields]
	search_fields = ("product_name","site")

class FeaturesAdmin(admin.ModelAdmin):
	list_filter = ("product","date")

class GoodTextAdmin(admin.ModelAdmin):
	list_filter = ("product",)

admin.site.register(Product,ProductAdmin)
admin.site.register(Scraping_Url,Scraping_UrlAdmin)
admin.site.register(Features,FeaturesAdmin)
admin.site.register(GoodText,GoodTextAdmin)
