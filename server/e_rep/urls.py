from django.urls import path
from .views import index, Product_List, Product_Details

urlpatterns = [
    path('', index, name='index'),
    path('products', Product_List.as_view(), name = "product_list"),
    path('products/<int:pk>', Product_Details.as_view(), name = "product_details"),
]