# install venv: 
    sudo apt-get install python3-venv

# create & activate virtual env :
	python3 -m venv my_venv
	source my_venv/bin/activate

-- inside the venv :
 
# install all requirements : 
    pip3 install -r requirements.txt

# create django project
    django-admin startproject project_name

# start django server
    python3 manage.py runserver

# create new django app
    python3 manage.py startapp app_name

# sync database for the first time
    python3 manage.py migrate

# create a super user
    python3 manage.py createsuperuser --email rassem.contact@gmail.com --username admin

# configure the new app
    - settings.py :
        * add 'rest_framework', 'app_name' to INSTALLED_APPS
	- urls.py :
        * from django.urls import path, include
        * add path('app_name/',include('app_name.urls')) to urlpatterns

# create the urls.py file in the app_name directory

# create a template
    * create "templates" directory and add it to settings.py : TEMPLATES/DIR
        os.path.join(BASE_DIR,"templates")
    * add the html files 

# home page view:
    - app_name/views.py
  
        def index(request):
            return render(request,'index.html')

    - app_name/url.py

        from django.urls import path
        from .views import index

        urlpatterns = [
            path('', index, name='index')
        ]

# create a model

    - app_name/models.py

        class Book(models.Model):
            title = models.CharField(max_length=256,null=True,blank=True)
            author = models.CharField(max_length=256,null=True,blank=True)
            year = models.PositiveIntegerField(default=0,null=True,blank=True)
            price = models.FloatField(default=1.0,null=True,blank=True)
            description = models.TextField(max_length=256,null=True,blank=True)
            bestseller = models.BooleanField(default=False)

    - app_name/admin.py : register the model, to use it in django admin interface
            
        from .models import Book
        admin.site.register(Book)
    
    - make the migrations
        python3 manage.py makemigrations // generate SQL commands
        python3 manage.py migrate // execute the SQL commands
    
    - add a serializer:

        create app_name/serializers.py
  
            from rest_framework import serializers
            from .models import Book

            class BookSerializer(serializers.ModelSerializer):
            class Meta:
                model = Book
                fields = '__all__' #(title,author...etc)

# Rest API View

    - app_name/views.py

        from django.http import JsonResponse
        from rest_framework.views import APIView
        from rest_framework import status
        from .models import Book
        from .serializers import BookSerializer

        class BookList(APIView):
        
            def get(self,request,format=None):
                books = Book.objects.all()
                serializer = BookSerializer(books,many=True)
                return JsonResponse(serializer.data, safe=False)
        
    - app_name/url.py

        add path('books/', BookList.as_view(), name = "book-list")

# useful commands

- stop server :
    ps auxw | grep runserver
    kill PID

# run standalone scripts
    pip3 install django-extensions
    add it to INSTALLED_APPS and set RUNSCRIPT_CHDIR_POLICY
    put the file.py in a scripts directory
    a valid script must have a run(*args) method
    python3 manage.py runscript data_loader --script-args "arg1"

# use data base shell:
    sudo apt-get install sqlite3
    python3 manage.py dbshell

